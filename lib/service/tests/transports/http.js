/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const EventEmitter = require('events');
const http = require('http');

const { expect } = require('chai');
const sinon = require('sinon');

const HttpTransport = require('../../src/transports/http');

describe('Http Transport', () => {
	let createHttpServerStub, httpServerMock;

	beforeEach(() => {
		createHttpServerStub = sinon.stub(http, 'createServer');

		httpServerMock = {
			listen: sinon.stub(),
			address: sinon.stub(),
			close: sinon.stub()
		};
	});

	afterEach(() => {
		createHttpServerStub.restore();
	});

	describe('constructor()', () => {
		it('should create server', () => {
			new HttpTransport(1234);

			sinon.assert.calledWithExactly(createHttpServerStub, sinon.match.func);
		});

		it('should create request listener', () => {
			new HttpTransport(1234);

			const requestListener = createHttpServerStub.args[0][0];

			const request = {
				on: sinon.stub()
			};

			requestListener(request);

			sinon.assert.calledWithExactly(request.on, 'data', sinon.match.func);
			sinon.assert.calledWithExactly(request.on, 'end', sinon.match.func);
		});
	});

	describe('get port()', () => {
		it('should return port', () => {
			createHttpServerStub.returns(httpServerMock);
			const transport = new HttpTransport();

			transport.start();

			httpServerMock.address.returns({port: 1234});

			expect(transport.port).to.be.equal(1234);
		});
	});

	describe('start()', () => {
		it('should listen for requests', () => {
			createHttpServerStub.returns(httpServerMock);
			let transport = new HttpTransport(1234);

			transport.start();

			sinon.assert.calledWithExactly(httpServerMock.listen, 1234, sinon.match.func);
		});
	});

	describe('stop()', () => {
		it('should stop listening', () => {
			createHttpServerStub.returns(httpServerMock);
			let transport = new HttpTransport(1234);

			transport.stop();

			sinon.assert.calledWithExactly(httpServerMock.close, sinon.match.func);
		});
	});

	describe('Incoming Messages events', () => {
		let transport, requestListener, requestMock, responseMock, serverMock;

		beforeEach(() => {
			serverMock = {
				handle: sinon.stub()
			};

			transport = new HttpTransport();
			transport._attach(serverMock);

			requestListener = createHttpServerStub.args[0][0];

			requestMock = new EventEmitter();

			responseMock = {
				writeHead: sinon.stub(),
				end: sinon.stub()
			};
		});

		describe('#end', () => {
			it('should create request from message', () => {
				requestListener(requestMock, responseMock);

				requestMock.emit('data', Buffer.from(JSON.stringify({foo: 'bar'})));
				requestMock.emit('end');

				sinon.assert.calledWithExactly(serverMock.handle, {foo: 'bar'}, sinon.match.object);
			});

			it('should listen for responses', () => {
				requestListener(requestMock, responseMock);

				requestMock.emit('data', Buffer.from(JSON.stringify({foo: 'bar'})));
				requestMock.emit('end');

				serverMock.handle.args[0][1].send('Hello world');

				sinon.assert.calledWithExactly(responseMock.end, JSON.stringify('Hello world'));
				sinon.assert.calledOnce(responseMock.writeHead);
				sinon.assert.callOrder(responseMock.writeHead, responseMock.end);
			});
		});
	});
});