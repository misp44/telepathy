/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const EventEmitter = require('events');

const {promisify} = require('util');
const {expect} = require('chai');
const sinon = require('sinon');

const WebSocketTransport = require('../../src/transports/ws');

const wait = promisify(setTimeout);

describe('Web Socket Transport', () => {
	let WebSocketServerMockClass, webSocketServerConstructorStub, webSocketServerCloseStub, webSocketMock, serverMock;
	let transport;

	beforeEach(() => {
		WebSocketServerMockClass = class WebSocketServerMock extends EventEmitter {
			constructor(options, callback) {
				super();

				webSocketServerConstructorStub(options, callback);

				this.clients = new Set();
			}

			close() {
				return webSocketServerCloseStub();
			}
		};

		webSocketServerCloseStub = sinon.stub();
		webSocketServerConstructorStub = sinon.stub();
		webSocketServerConstructorStub.callsFake((options, callback) => setTimeout(callback, 1));

		webSocketMock = {
			close: sinon.stub(),
			send: sinon.stub(),
			on: sinon.stub(),
			terminate: sinon.stub(),
			ping: sinon.stub()
		};

		serverMock = {
			handle: sinon.stub()
		};

		transport = new WebSocketTransport({port: 9000}, WebSocketServerMockClass);
	});

	afterEach(() => {
		if (transport.started) {
			transport.stop();
		}
	});

	describe('start()', () => {
		it('should create Web Socket server', async () => {
			await transport.start();

			sinon.assert.calledWithExactly(
				webSocketServerConstructorStub,
				sinon.match.has('port', 9000),
				sinon.match.func
			);
		});

		it('should throw an exception if already started', async () => {
			await transport.start();

			let error;

			try {
				await transport.start();
			} catch (err) {
				error = err;
			}

			expect(error).to.be.instanceOf(Error).with.property('message', 'Already started');
		});
	});

	describe('stop()', () => {
		it('should stop Web Socket server', async () => {
			await transport.start();

			transport.stop();

			sinon.assert.calledOnce(webSocketServerCloseStub);
		});
	});

	describe('get connections()', () => {
		it('should return number of connected clients', async () => {
			await transport.start();

			transport._wss.clients = {size: 5};

			expect(transport.connections).to.be.equal(5);
		});
	});

	describe('Web Socket Server events', () => {
		describe('#connection', () => {
			it('should listen for connections', async () => {
				await transport.start();

				transport._wss.emit('connection', webSocketMock);

				sinon.assert.calledWithExactly(webSocketMock.on, 'message', sinon.match.func);
			});

			context('limit of clients has been reached', () => {
				it('should reject connection', async () => {
					transport = new WebSocketTransport({port: 9000, connectionsLimit: 10}, WebSocketServerMockClass);

					await transport.start();

					transport._wss.clients = {size: 10};

					transport._wss.emit('connection', webSocketMock);

					sinon.assert.calledOnce(webSocketMock.close);
					sinon.assert.notCalled(webSocketMock.on);
				});
			});
		});
	});

	describe('Web Socket events', () => {
		describe('#message', () => {
			let onMessageListener;

			beforeEach(async () => {
				webSocketMock.on.callsFake((event, callback) => {
					if (event === 'message') {
						onMessageListener = callback;
					}
				});

				transport._attach(serverMock);
				
				await transport.start();
			});

			it('should create request from message', () => {
				transport._wss.emit('connection', webSocketMock);

				onMessageListener(JSON.stringify({channel: 'abc', payload: 'foobar'}));

				sinon.assert.calledWithExactly(serverMock.handle, 'foobar', sinon.match.object);
			});

			it('should send response', () => {
				transport._wss.emit('connection', webSocketMock);
				let response;

				serverMock.handle.callsFake((request, res) => response = res);
				onMessageListener(JSON.stringify({channel: 'abc', payload: 'foobar'}));

				response.send(1234);

				sinon.assert.calledWithExactly(webSocketMock.send, JSON.stringify({channel: 'abc', response: 1234}));
			});

			it('should set alive flag', async () => {
				transport._wss.emit('connection', webSocketMock);

				expect(webSocketMock).to.have.property('isAlive', true);				
			});
		});

		describe('#pong', () => {
			let onPongListener;

			beforeEach(async () => {
				transport = new WebSocketTransport({heartBeatInterval: 20}, WebSocketServerMockClass);

				webSocketMock.on.callsFake((event, callback) => {
					if (event === 'pong') {
						onPongListener = callback;
					}
				});

				transport._attach(serverMock);

				await transport.start();
			});

			it('should set alive flag', async () => {
				transport._wss.emit('connection', webSocketMock);
				transport._wss.clients.add(webSocketMock);

				expect(webSocketMock).to.have.property('isAlive', true);

				await wait(20);

				expect(webSocketMock).to.have.property('isAlive', false);

				onPongListener();

				expect(webSocketMock).to.have.property('isAlive', true);

				sinon.assert.notCalled(webSocketMock.terminate);
			});

			it('should be terminated if pong is not emitted', async () => {
				transport._wss.emit('connection', webSocketMock);
				transport._wss.clients.add(webSocketMock);

				expect(webSocketMock).to.have.property('isAlive', true);

				await wait(20);

				expect(webSocketMock).to.have.property('isAlive', false);

				await wait(20);

				expect(webSocketMock).to.have.property('isAlive', false);

				sinon.assert.calledOnce(webSocketMock.terminate);
				expect(transport._wss.clients.has(webSocketMock)).to.be.false;
			});
		});
	});
});
