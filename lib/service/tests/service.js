/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const {expect} = require('chai');
const sinon = require('sinon');

const Service = require('../src/service');

describe('Service', () => {
	let responseCreatorMock, fooMethodListener, barMethodListener, testService;

	class TestService extends Service {
		constructor(config) {
			super(config, 'foo', 'bar');
		}

		async foo(...args) {
			return await fooMethodListener(...args);
		}

		bar(...args) {
			barMethodListener(...args);
		}
	}

	beforeEach(() => {
		responseCreatorMock = {
			returns: sinon.stub(),
			throws: sinon.stub()
		};

		fooMethodListener = sinon.stub();
		barMethodListener = sinon.stub();

		testService = new TestService({responseCreator: responseCreatorMock});
	});

	describe('invoke()', () => {
		it('should call "foo" method', async () => {
			await testService.invoke('foo', 'some', 'arguments');

			sinon.assert.calledWithExactly(fooMethodListener, 'some', 'arguments');
		});

		it('should call "bar" method', async () => {
			await testService.invoke('bar', 'some', 'arguments');

			sinon.assert.calledWithExactly(barMethodListener, 'some', 'arguments');
		});

		it('should return response', async () => {
			responseCreatorMock.returns.returns('Some response');

			fooMethodListener.resolves(1234);
			const response = await testService.invoke('foo');

			sinon.assert.calledWithExactly(responseCreatorMock.returns, 1234);
			expect(response).to.be.deep.equal('Some response');
		});

		it('should return error response if method thrown an error', async () => {
			responseCreatorMock.throws.returns('Some error response');

			fooMethodListener.rejects(1234);
			const response = await testService.invoke('foo');

			sinon.assert.calledWithExactly(responseCreatorMock.throws, 1234);
			expect(response).to.be.deep.equal('Some error response');
		});

		it('should return error response if method does not exist', async () => {
			responseCreatorMock.throws.returns('Some error response');

			const response = await testService.invoke('unknownMethod');

			sinon.assert.calledWithExactly(responseCreatorMock.throws, 'Method unknownMethod does not exist');
			expect(response).to.be.deep.equal('Some error response');
		});
	});
});