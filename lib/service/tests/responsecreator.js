/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const {expect} = require('chai');

const ResponseCreator = require('../src/responsecreator');

describe('Response Creator', () => {
	describe('returns', () => {
		const responseCreator = new ResponseCreator();

		const response = responseCreator.returns('foobar');

		expect(response).to.be.deep.equal({
			type: 'value',
			payload: 'foobar'
		});
	});

	describe('returns', () => {
		const responseCreator = new ResponseCreator();

		const response = responseCreator.throws('foobar');

		expect(response).to.be.deep.equal({
			type: 'error',
			payload: 'Internal service error'
		});
	});
});
