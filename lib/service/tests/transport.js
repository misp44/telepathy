/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const {expect} = require('chai');

const Transport = require('../src/transport');

describe('Transport', () => {
	describe('_attach()', () => {
		it('should attach server', () => {
			const transport = new Transport();
			const server = 'SERVER';

			transport._attach(server);

			expect(transport._server).to.be.equal(server);
		});
	});

	describe('start()', () => {
		it('should throw an unimplemented error', () => {
			const transport = new Transport();

			expect(() => transport.start()).to.throw(TypeError).with.property('message', 'Unimplemented method.');
		});
	});
});