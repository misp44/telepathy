/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const sinon = require('sinon');

const Server = require('../src/server');

describe('Server', () => {
	let transport, transport2, service;

	beforeEach(() => {
		transport = {
			start: sinon.stub(),
			stop: sinon.stub(),
			_attach: sinon.stub()
		};

		transport2 = {
			start: sinon.stub(),
			stop: sinon.stub(),
			_attach: sinon.stub()
		};

		service = {
			invoke: sinon.stub()
		};
	});

	describe('start()', () => {
		it('should start all transports', () => {
			const server = new Server([transport, transport2], service);

			server.start();

			sinon.assert.calledOnce(transport.start);
			sinon.assert.calledOnce(transport2.start);
		});
	});

	describe('stop()', () => {
		it('should start all transports', () => {
			const server = new Server([transport, transport2], service);

			server.stop();

			sinon.assert.calledOnce(transport.stop);
			sinon.assert.calledOnce(transport2.stop);
		});
	});

	describe('constructor()', () => {
		it('should attach transports to server', () => {
			const server = new Server([transport, transport2], service);

			sinon.assert.calledWithExactly(transport._attach, server);
			sinon.assert.calledWithExactly(transport2._attach, server);
		});
	});

	describe('handle()', () => {
		it('should invoke service method', async () => {
			service.invoke.resolves(12345);

			const server = new Server([transport], service);

			const request = {method: 'someMethod', args: ['foobar']};
			const response = {
				send: sinon.stub()
			};

			await server.handle(request, response);

			sinon.assert.calledWithExactly(service.invoke, 'someMethod', 'foobar');
			sinon.assert.calledWithExactly(response.send, 12345);
		});
	});
});