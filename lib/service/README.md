# Telepathy Server

Telepathy is a lightweight Node.js library for remote procedure calls using proxy objects.

## Getting Started

To work with Telepathy you must have installed Node.js version 8 or higher.

## Installation

```bash
npm install --save telepathy-server
```

## Usage

Let's write simple service:

```javascript
const {Service, Server, Transports: {WebSocketTransport}} = require('telepathy-server');

class DemoService extends Service {
	constructor() {
		// You have to define which methods should be exposed
		super({}, 'welcome');
	}

	welcome(name) {
	    return `Hello, ${name}.`;
	}
}

const service = new DemoService();
const transports = [new WebSocketTransport({port: 9001})];
const server = new Server(transports, service);

server.start();
```

## See also

- [Telepathy Client](https://www.npmjs.com/package/telepathy-client)

- [Gitlab repository](https://gitlab.com/misp44/telepathy/)

- [Example](https://gitlab.com/misp44/telepathy/tree/master/demo)

## License

This project is licensed under the [MIT License](LICENSE).
