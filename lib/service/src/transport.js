/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

/**
 * Base class for transports.
 */
class Transport {
	/**
	 * Attachs transport to the Server.
	 *
	 * @param {Server} server
	 * @package
	 */
	_attach(server) {
		this._server = server;
	}

	/**
	 * Starts listening for connections.
	 *
	 * @returns {Promise}
	 * @abstract
	 */
	start() {
		throw new TypeError('Unimplemented method.');
	}
}

module.exports = Transport;
