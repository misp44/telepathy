/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const ResponseCreator = require('./responsecreator');

/**
 * Base class for services.
 */
class Service {
	/**
	 * @param {ServiceConfig} config
	 * @param {String} exposed names of exposed methods
	 */
	constructor(config, ...exposed) {
		this._exposed = new Map();
		this._responseCreator = config.responseCreator || /* istanbul ignore next */ new ResponseCreator();

		for (const method of exposed) {
			this._exposed.set(method, this[method]);
		}
	}

	/**
	 * Invokes service method and returns an response.
	 *
	 * @param {String} method
	 * @param {*} args
	 * @returns {Promise.<*>}
	 */
	async invoke(method, ...args) {
		let func = this._exposed.get(method);

		if (!func) {
			return this._responseCreator.throws(`Method ${ method } does not exist`);
		}

		func = func.bind(this);

		let value;

		try {
			value = await func(...args);
		} catch (error) {
			return this._responseCreator.throws(error);
		}

		return this._responseCreator.returns(value);
	}
}

module.exports = Service;

/**
 * @typedef {Object} ServiceConfig
 * @property {ResponseCreator} [responseCreator]
 */
