/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const Transport = require('../transport');

class AmqpTransport extends Transport {
	/**
	 * @param {Object} config
	 * @param {AMQP.} [AMQP]
	 */
	constructor(config, /* istanbul ignore next */ AMQP = require('amqplib')) {
		super();

		this._AMQP = AMQP;
		this._config = config;
	}

	async start() {
		this._connection = await this._AMQP.connect(this._config.url, this._config.socketOptions);
		this._channel = await this._connection.createChannel();

		const queue = this._channel.assertQueue(this._config.queue, {durable: false});

		this._channel.consume(queue.queue, async message => await this._handle(message), {noAck: true});
	}

	async stop() {
		await this._channel.close();
		await this._connection.close();
	}

	async _handle(message) {
		const request = JSON.parse(message.content.toString());
		const replyQueue = message.properties.replyTo;
		const correlationId = message.properties.correlationId;
		const response = new AmqpResponse(this._channel, replyQueue, correlationId);

		await this._server.handle(request, response);
	}
}

module.exports = AmqpTransport;

class AmqpResponse {
	constructor(channel, replyQueue, correlationId) {
		this._channel = channel;
		this._replyQueue = replyQueue;
		this._correlationId = correlationId;
	}

	send(response) {
		this._channel.publish(
			'',
			this._replyQueue,
			Buffer.from(JSON.stringify(response)),
			{
				correlationId: this._correlationId
			}
		);
	}
}
