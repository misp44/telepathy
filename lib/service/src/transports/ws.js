/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const Transport = require('../transport');

const DEFAULT_HEARTBEAT_INTERVAL = 5000;

class WebSocketTransport extends Transport {
	/**
	 * @param {Object} config
	 * @param {Number} config.port
	 * @param {Number} [config.connectionsLimit]
	 * @param {Number} [config.heartBeatInterval]
	 * @param {WebSocket.Server.} WebSocket
	 */
	constructor(config, /* istanbul ignore next */ WebSocket = require('ws').Server) {
		super();

		this._config = config;

		this._port = config.port;

		this._connectionsLimit = config.connectionsLimit;

		this._WebSocket = WebSocket;
	}

	/**
	 * @override
	 */
	async start() {
		if(this._started) {
			throw new Error('Already started');
		}

		this._started = true;

		this._wss = await this._createWebSocketServer(this._port);
		this._wss.on('connection', ws => this._handleConnection(ws));
		this._heartBeat();
	}

	/**
	 * @override
	 */
	stop() {
		this._started = false;

		clearInterval(this._heartBeatInterval);

		return new Promise(resolve => this._wss.close(resolve));
	}

	/**
	 * Returns number of connections.
	 * 
	 * @returns {Number}
	 */
	get connections() {
		return this._wss.clients.size;
	}

	/**
	 * Returns status of transport.
	 * 
	 * @returns {Boolean}
	 */
	get started() {
		return this._started;
	}

	/**
	 * Listens for client messages.
	 *
	 * @param {WebSocket} ws
	 * @returns {Promise}
	 * @private
	 */
	_handleConnection(ws) {
		ws.isAlive = true;

		if (this._connectionsLimit && this.connections >= this._connectionsLimit) {
			return ws.close();
		}

		ws.on('message', async message => {
			const request = JSON.parse(message);
			const {channel} = request;
			const response = new WebSocketResponse(ws, channel);

			await this._server.handle(request.payload, response);
		});

		ws.on('pong', () => ws.isAlive = true);
	}

	/**
	 * Starts health checking of connected sockets.
	 *
	 * @private
	 */
	_heartBeat() {
		this._heartBeatInterval = setInterval(() => {
			for (const ws of this._wss.clients) {
				if (ws.isAlive === false) {
					ws.terminate();
					this._wss.clients.delete(ws);
				}

				ws.isAlive = false;

				ws.ping(() => {});
			}
		}, this._config.heartBeatInterval || DEFAULT_HEARTBEAT_INTERVAL);
	}

	/**
	 * @param {Number} port
	 * @returns {Promise.<WebSocket.Server>}
	 * @private
	 */
	_createWebSocketServer(port) {
		return new Promise(resolve => {
			const wss = new this._WebSocket({port}, () => resolve(wss));
		});
	}
}

module.exports = WebSocketTransport;

class WebSocketResponse {
	/**
	 * @param {WebSocket} ws
	 * @param {String} channel
	 */
	constructor(ws, channel) {
		this._ws = ws;
		this._channel = channel;
	}

	/**
	 * @param {*} response
	 */
	send(response) {
		this._ws.send(JSON.stringify({
			channel: this._channel,
			response
		}));
	}
}