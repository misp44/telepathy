/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const {promisify} = require('util');

const http = require('http');

const Transport = require('../transport');

class HttpTransport extends Transport {
	/**
	 * @param {Number} port
	 */
	constructor(port) {
		super();

		this._port = port;

		this._http = http.createServer((request, response) => {
			let buffers = [];

			request.on('data', buffer => buffers.push(buffer));
			request.on('end', async () => {
				const request = Buffer.concat(buffers).toString();

				await this._server.handle(JSON.parse(request), new HttpResponse(response));
			});
		});
	}

	/**
	 * @override
	 */
	async start() {
		await promisify(this._http.listen).call(this._http, this._port);
	}

	/**
	 * @override
	 */
	async stop() {
		await promisify(this._http.close).call(this._http);
	}

	/**
	 * @override
	 */
	get port() {
		return this._http.address().port;
	}
}

module.exports = HttpTransport;

class HttpResponse {
	/**
	 * @param {http.ServerResponse} response
	 */
	constructor(response) {
		this._response = response;
	}

	/**
	 * @param {*} response
	 */
	send(response) {
		this._response.writeHead(200, {'Content-Type': 'application/json'});
		this._response.end(JSON.stringify(response));
	}
}