/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

/**
 * Server for running services.
 */
class Server {
	/**
	 * @param {Transport[]} transports
	 * @param {Service} service
	 */
	constructor(transports, service) {
		/**
		 * @type {Transport[]}
		 * @private
		 */
		this._transports = transports;

		/**
		 * @type {Service}
		 * @private
		 */
		this._service = service;

		for (const transport of transports) {
			transport._attach(this);
		}
	}

	/**
	 * Starts listening to connections.
	 *
	 * @returns {Promise}
	 */
	async start() {
		await Promise.all(this._transports.map(transport=>transport.start()));
	}

	/**
	 * Stops listening to connections.
	 *
	 * @returns {Promise}
	 */
	async stop() {
		await Promise.all(this._transports.map(transport=>transport.stop()));
	}

	/**
	 * Handles request and sends response.
	 *
	 * @param {{method:string, args:Array}} request
	 * @param {Response} response
	 * @returns {Promise}
	 */
	async handle(request, response) {
		const ret = await this._service.invoke(request.method, ...request.args);

		response.send(ret);
	}
}

module.exports = Server;
