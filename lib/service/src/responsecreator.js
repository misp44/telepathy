/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

/**
 * Creator of responses for client.
 */
class ResponseCreator {
	/**
	 * Creates response with returned value.
	 *
	 * @param {*} value
	 */
	returns(value) {
		return {
			type: 'value',
			payload: value
		};
	}

	/**
	 * Creates resposne with an error.
	 *
	 * @param {*} error
	 */
	throws(/* error */) {
		return {
			type: 'error',
			payload: 'Internal service error'
		};
	}
}

module.exports = ResponseCreator;
