/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const EventEmitter = require('events');
const http = require('http');

const {expect} = require('chai');
const sinon = require('sinon');

const HTTPTransport = require('../../src/transports/http');

describe('HTTP Transport', () => {
	let httpRequestStub;

	beforeEach(() => {
		httpRequestStub = sinon.stub(http, 'request');
	});

	afterEach(() => {
		httpRequestStub.restore();
	});

	describe('send()', () => {
		context('host exists', () => {
			let clientRequestMock;

			beforeEach(() => {
				clientRequestMock = {
					write: sinon.stub(),
					end: sinon.stub()
				};

				httpRequestStub.returns(clientRequestMock);
			});

			it('should send request', async () => {
				const transport = new HTTPTransport();

				transport.connect('host.dev', 9000);

				transport.send({foo: 'bar'});

				sinon.assert.calledWith(httpRequestStub, {
					hostname: 'host.dev',
					port: 9000,
					method: 'POST'
				});

				sinon.assert.calledOnce(clientRequestMock.write);
				expect(clientRequestMock.write.args[0][0]).to.be.equal(JSON.stringify({foo: 'bar'}));
				sinon.assert.calledOnce(clientRequestMock.end);
				sinon.assert.callOrder(clientRequestMock.write, clientRequestMock.end);
			});

			it('should get response', async () => {
				let callback;
				const responseMock = new EventEmitter();

				httpRequestStub.callsFake((options, cb) => {
					callback = cb;

					return clientRequestMock;
				});

				const transport = new HTTPTransport();

				transport.connect('host.dev', 9000);

				const responsePromise = transport.send({foo: 'bar'});

				callback(responseMock);
				responseMock.emit('data', Buffer.from('{"foo"'));
				responseMock.emit('data', Buffer.from(': "bar"}'));
				responseMock.emit('end');

				expect(await responsePromise).to.be.deep.equal({foo: 'bar'});
			});
		});
	});
});
