/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const EventEmitter = require('events');

const {expect} = require('chai');
const sinon = require('sinon');

const WebSocketTransport = require('../../src/transports/ws');

describe('Web Socket Transport', () => {
	let WebSocketMockClass, webSocketConstructorSpy, webSocketMock;

	beforeEach(() => {
		WebSocketMockClass = class WebSocketMock extends EventEmitter {
			constructor(address) {
				super();

				webSocketConstructorSpy(address);

				Object.assign(this, webSocketMock);
			}
		};

		webSocketConstructorSpy = sinon.spy();

		webSocketMock = {
			send: sinon.spy()
		};
	});

	describe('connect()', () => {
		context('host exists', () => {
			it('should connect', async () => {
				const transport = new WebSocketTransport(WebSocketMockClass);

				const connectionPromise = transport.connect('ws://foo.bar');

				transport._ws.emit('open');
				await connectionPromise;

				sinon.assert.calledWithExactly(webSocketConstructorSpy, 'ws://foo.bar');
			});
		});

		context('host unreachable', () => {
			it.skip('should throw an error', async () => {
				const transport = new WebSocketTransport(WebSocketMockClass);

				// Todo

				let error;

				try {
					await transport.connect('ws://foo.bar');
				} catch (err) {
					error = err;
				}

				expect(error).to.be.instanceOf(Error)
					.with.property('message', 'Cannot connect to: "ws://foo.bar"');
			});
		});
	});

	describe('send()', () => {
		context('connected', () => {
			let transport;

			beforeEach(async () => {
				transport = new WebSocketTransport(WebSocketMockClass);
				const connectionPromise = transport.connect('ws://foo.bar');

				transport._ws.emit('open');
				await connectionPromise;
			});

			it('should send request', async () => {
				transport.send({foo: 'bar'});

				sinon.assert.calledOnce(webSocketMock.send);

				const message = JSON.parse(webSocketMock.send.args[0][0]);

				expect(message).to.have.keys('channel', 'payload');
				expect(message.payload).to.be.deep.equal({foo: 'bar'});
			});

			it('should listen to response', async () => {
				const responsePromise = transport.send({foo: 'bar'});
				const message = JSON.parse(webSocketMock.send.args[0][0]);

				transport._ws.emit('message', JSON.stringify({channel: message.channel, response: 'hello world'}));

				const response = await responsePromise;

				expect(response).to.be.deep.equal('hello world');
			});
		});
	});
});