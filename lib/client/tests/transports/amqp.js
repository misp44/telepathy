/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const {expect} = require('chai');
const sinon = require('sinon');

const AmqpTransport = require('../../src/transports/amqp');

describe('AMQP Transport', () => {
	let AmqpMock;
	let amqpConnectionMock;
	let amqpChannelMock;
	let amqpQueueMock;

	beforeEach(() => {
		AmqpMock = {
			connect: sinon.stub()
		};

		amqpConnectionMock = {
			createChannel: sinon.stub()
		};

		amqpChannelMock = {
			assertQueue: sinon.stub(),
			publish: sinon.stub(),
			consume: sinon.stub()
		};

		amqpQueueMock = {
			queue: 'testQueue'
		};

		AmqpMock.connect.resolves(amqpConnectionMock);
		amqpConnectionMock.createChannel.resolves(amqpChannelMock);
		amqpChannelMock.assertQueue.resolves(amqpQueueMock);
	});

	describe('connect()', () => {
		it('should create response queue', async () => {
			const url = 'amqp://example';
			const queue = 'example-service-queue';
			const socketOptions = {};

			const amqpTransport = new AmqpTransport(AmqpMock);

			await amqpTransport.connect(url, queue, socketOptions);

			sinon.assert.calledWithExactly(AmqpMock.connect, url, socketOptions);
			sinon.assert.calledOnce(amqpConnectionMock.createChannel);
			sinon.assert.calledOnce(amqpChannelMock.assertQueue);
			sinon.assert.calledWithExactly(amqpChannelMock.assertQueue, '', sinon.match.object);
			// Todo: Assert consume!
			expect(amqpChannelMock.assertQueue.args[0][1]).to.be.deep.equal({exclusive: true});
		});
	});

	describe('send()', () => {
		const queue = 'example-service-queue';
		let transport;
		let consumeCallback;

		beforeEach(async () => {
			transport = new AmqpTransport(AmqpMock);
			amqpChannelMock.consume.callsFake((queue, callback) => consumeCallback = callback);

			await transport.connect('amqp://example', queue, {});
		});

		it('should send request', async () => {
			transport.send({foo: 'bar'});

			sinon.assert.calledWithExactly(amqpChannelMock.publish, '', queue, sinon.match.any, sinon.match.object);

			expect(amqpChannelMock.publish.args[0][2]).to.be.instanceOf(Buffer);
			expect(JSON.parse(amqpChannelMock.publish.args[0][2])).to.be.deep.equal({foo:'bar'});
		});

		it('should listen for response', async () => {
			const publishPromise =  new Promise(resolve => amqpChannelMock.publish.callsFake(resolve));
			const responsePromise = transport.send({foo: 'bar'});

			await publishPromise;

			const {replyTo, correlationId} = amqpChannelMock.publish.args[0][3];

			consumeCallback({
				properties: {correlationId},
				content: Buffer.from('"hello world"')
			});

			const response = await responsePromise;

			expect(replyTo).to.be.equal(amqpQueueMock.queue);
			expect(correlationId).to.be.a.string;
			expect(response).to.be.deep.equal('hello world');
		});
	});
});