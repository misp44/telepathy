/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const {expect} = require('chai');
const sinon = require('sinon');

const Client = require('../src/client');

describe('Client', () => {
	let client, transport, responseHandler;

	beforeEach(() => {
		transport = {
			send: sinon.stub()
		};

		responseHandler = {
			handle: sinon.stub()
		};

		client = new Client(transport, responseHandler);
	});

	describe('invoke()', () => {
		it('should invoke method', async () => {
			transport.send.resolves({foo: 'bar'});
			responseHandler.handle.resolves('foobar');

			const value = await client.invoke('someMethod', 'some argument', 123);

			sinon.assert.calledOnce(transport.send);

			expect(transport.send.args[0][0]).to.be.deep.equal({
				method: 'someMethod',
				args: ['some argument', 123]
			});

			expect(value).to.be.equal('foobar');

			sinon.assert.calledWithExactly(responseHandler.handle, {foo: 'bar'});
		});
	});

	describe('getProxy()', () => {
		it('should return function', () => {
			const proxy = client.getProxy();

			expect(proxy.someMethod).to.be.a('function');
		});

		it('should invoke method', async () => {
			transport.send.resolves({foo: 'bar'});
			responseHandler.handle.resolves('foobar');

			const proxy = client.getProxy();

			const value = await proxy.someMethod('some argument', 123);

			sinon.assert.calledOnce(transport.send);

			expect(transport.send.args[0][0]).to.be.deep.equal({
				method: 'someMethod',
				args: ['some argument', 123]
			});

			expect(value).to.be.equal('foobar');
		});

		it('should work in async functions', async () => {
			async function func() {
				return client.getProxy();
			}

			const proxy = await func();

			await proxy.someMethod('some argument', 123);

			expect(transport.send.args[0][0]).to.be.deep.equal({
				method: 'someMethod',
				args: ['some argument', 123]
			});
		});
	});
});