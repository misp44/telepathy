/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const {expect} = require('chai');

const ResponseHandler = require('../src/responsehandler');

describe('Response Handler', () => {
	let responseHandler;

	beforeEach(() => {
		responseHandler = new ResponseHandler();
	});

	describe('handle()', () => {
		it('should return a value if response type is a value', () => {
			const value = responseHandler.handle({
				type: 'value',
				payload: {foo: 'bar'}
			});

			expect(value).to.be.deep.equal({foo: 'bar'});
		});

		it('should throw an error type is an error', () => {
			expect(() => responseHandler.handle({
				type: 'error',
				payload: 'Houston we have a problem'
			})
			).to.throw(Error).with.property('message', 'Houston we have a problem');
		});

		it('should throw an invalid response error if type is undefined', () => {
			expect(() => responseHandler.handle({
				payload: 'Houston we have a problem'
			})
			).to.throw(Error).with.property('message', 'Invalid response');
		});
	});
});
