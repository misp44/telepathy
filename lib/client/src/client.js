/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const ResponseHandler = require('./responsehandler');

/**
 * Class for calling remote procedures via specified {@link Transport}.
 */
class Client {
	/**
	 * @param {Transport} transport
	 * @param {ResponseHandler} [responseHandler]
	 */
	constructor(transport, /* istanbul ignore next */ responseHandler = new ResponseHandler()) {
		this._transport = transport;
		this._responseHandler = responseHandler;
	}

	/**
	 * Invokes service method.
	 *
	 * @param {String} method
	 * @param {*} args
	 * @returns {Promise.<*>}
	 * @throws {*}
	 */
	async invoke(method, ...args) {
		const response = await this._transport.send({method, args});

		return this._responseHandler.handle(response);
	}

	/**
	 * Returns proxy object of the service.
	 *
	 * @returns {Proxy}
	 */
	getProxy() {
		return new Proxy({}, {
			get: (target, propKey) => {
				// Needed for usage in async functions.
				if (propKey === 'then') {
					return this;
				}

				return (...args) => this.invoke(propKey, ...args);
			}
		});
	}
}

module.exports = Client;
