/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

/**
 * Handler for responses from service.
 */
class ResponseHandler {
	/**
	 * Creates value from the response or throws an error.
	 *
	 * @param {Object} response
	 * @returns {*}
	 * @throws
	 */
	handle(response) {
		if (response.type === 'value') {
			return response.payload;
		} else if (response.type === 'error') {
			throw new Error(response.payload);
		} else {
			throw new Error('Invalid response');
		}
	}
}

module.exports = ResponseHandler;
