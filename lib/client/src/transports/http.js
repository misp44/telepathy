/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const EventEmitter = require('events');
const http = require('http');

/**
 * Sends requests to service via HTTP - without pernament connection.
 */
class HttpTransport extends EventEmitter {
	/**
	 * Connects to server.
	 *
	 * @param {String} hostname
	 * @param {Number} port
	 */
	connect(hostname, port) {
		this._hostname = hostname;
		this._port = port;
	}

	/**
	 * Send request and wait for response.
	 * 
	 * @param {Object} payload
	 * @returns {Promise.<*>}
	 * @override
	 */
	send(payload) {
		return this._request({
			hostname: this._hostname,
			port: this._port,
			method: 'POST'
		}, payload);
	}

	/**
	 * @param {Object} options
	 * @param {Object} payload
	 * @returns {Promise.<*>}
	 * @private
	 */
	_request(options, payload) {
		return new Promise(resolve => {
			const req = http.request(options, res => {
				let buffers = [];

				res.on('data', buffer => buffers.push(buffer));
				res.on('end', () => {
					const response = Buffer.concat(buffers).toString();

					resolve(JSON.parse(response.toString()));
				});
			});

			req.write(JSON.stringify(payload));
			req.end();
		});
	}
}

module.exports = HttpTransport;
