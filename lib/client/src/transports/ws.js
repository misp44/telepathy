/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const uuidv4 = require('uuid/v4');

const EventEmitter = require('events');

/**
 * Connects to server via WebSockets.
 */
class WebSocketTransport extends EventEmitter {
	/**
	 * @param {WebSocket.} WebSocket
	 */
	constructor(/* istanbul ignore next */ WebSocket = require('ws')) {
		super();

		/**
		 * @type {WebSocket.}
		 * @private
		 */
		this._WebSocket = WebSocket;
	}

	/**
	 * Connects to server.
	 *
	 * @param {String} address
	 * @returns {Promise}
	 */
	connect(address) {
		this._ws = new this._WebSocket(address);
		this._ws.on('message', message => {
			const {channel, response} = JSON.parse(message);

			this.emit(channel, response);
		});

		return new Promise(resolve => this._ws.on('open', () => resolve()));
	}

	/**
	 * Send request and wait for response.
	 *
	 * @param {*} payload
	 * @returns {Promise.<*>}
	 * @override
	 */
	send(payload) {
		const channel = uuidv4();

		this._ws.send(JSON.stringify({channel, payload}));

		return new Promise(resolve => this.once(channel, resolve));
	}
}

module.exports = WebSocketTransport;
