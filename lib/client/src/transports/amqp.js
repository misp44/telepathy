/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const EventEmitter = require('events');
const uuidv4 = require('uuid/v4');

/**
 * Sends requests via AMQP queues.
 */
class AmqpTransport extends EventEmitter {
	/**
	 * @params {AMQP.}
	 */
	constructor(/* istanbul ignore next */ AMQP = require('amqplib')) {
		super();

		this._AMQP = AMQP;

		/**
		 * @type {Map.<String, Function>}
		 * @private
		 */
		this._correlationCallbacks = new Map();
	}

	/**
	 * Connects to AMQP server.
	 * 
	 * @param {String} url
	 * @param {String} queue
	 * @param {Object} socketOptions
	 */
	async connect(url, queue, socketOptions) {
		this._queue = queue;

		this._connection = await this._AMQP.connect(url, socketOptions);
		this._channel = await this._connection.createChannel();
		this._responseQueue = await this._channel.assertQueue('', {exclusive: true});

		this._channel.consume(this._responseQueue.queue, message => {
			const correlationId = message.properties.correlationId;
			const responseCallback = this._correlationCallbacks.get(correlationId);

			/* istanbul ignore else */
			if (responseCallback) {
				responseCallback(JSON.parse(message.content.toString()));
			} else {
				console.warn(`Cannot find callback for correlation '${correlationId}'`);
			}
		});
	}

	/**
	 * Send request and wait for response.
	 *
	 * @param {*} payload
	 * @returns {Promise.<void>}
	 */
	async send(payload) {
		const correlationId = uuidv4();
		let responseResolver;
		const responsePromise = new Promise(resolve => responseResolver = resolve);

		this._correlationCallbacks.set(correlationId, responseResolver);

		await this._channel.publish(
			'',
			this._queue,
			Buffer.from(JSON.stringify(payload)),
			{
				replyTo: this._responseQueue.queue,
				correlationId: correlationId
			}
		);

		return await responsePromise;
	}
}

module.exports = AmqpTransport;
