/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

module.exports = {
	Client: require('./client'),
	ResponseHandler: require('./responsehandler'),
	Transports: {
		HttpTransport: require('./transports/http'),
		WebSocketTransport: require('./transports/ws'),
		AmqpTransport: require('./transports/amqp')
	}
};
