# Telepathy Client

Telepathy is a lightweight Node.js library for remote procedure calls using proxy objects.

## Getting Started

To work with Telepathy you must have installed Node.js version 8 or higher.

## Installation

```bash
npm install --save telepathy-client
```

## Usage

```javascript
const {Client, Transports: {WebSocketTransport}} = require('telepathy-client');

(async () => {
	const transport = new WebSocketTransport();
	const client = new Client(transport);
	const service = client.getProxy();

	await transport.connect('ws://localhost:9001');

	console.log(await service.welcome('world'));
})();
```

## See also

- [Telepathy Server](https://www.npmjs.com/package/telepathy-server)

- [Gitlab repository](https://gitlab.com/misp44/telepathy/)

- [Example](https://gitlab.com/misp44/telepathy/tree/master/demo)

## License

This project is licensed under the [MIT License](LICENSE).
