# Contributing guidelines

Thanks for taking the time to contribute!

Before you make any change on this repository, a good idea would be if you first discuss your intention via issue.


## Issues

Ensure you not repeat existing issue. If you're unable to find similar issue, feel free to open a new.

If you are reporting a bug, do not forget to specify:

- operating system

- versions of library and dependencies

- steps to reproduce

- expected and actual results

All detailed information which will be helpful to understand an issue are welcome.


## Making changes

Firstly, create a topic branch (usually the master branch). The name of branch should be started with `topic/` and refer
to an issue (eg. `topic/103`). 


### Pull requests

The merge request could be merged only if fulfills 100% code coverage and code style.
