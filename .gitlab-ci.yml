image: node:8

.client-cache: &client-cache
  key: $CI_PIPELINE_ID-client
  policy: pull
  paths:
    - lib/client/node_modules

.service-cache: &service-cache
  key: $CI_PIPELINE_ID-service
  policy: pull
  paths:
    - lib/service/node_modules

.integration-cache: &integration-cache
  key: $CI_PIPELINE_ID-integrational
  policy: pull
  paths:
    - integration-tests/node_modules

stages:
  - prepare
  - tests
  - integration tests
  - release
  - security

dependency scanning:
  stage: security
  image: docker:stable
  variables:
    DOCKER_DRIVER: overlay2
  allow_failure: true
  services:
    - docker:stable-dind
  before_script:
    - apk add npm
    - (cd integration-tests && npm install)
    - (cd lib/client && npm install)
    - (cd lib/service && npm install)
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
      --env DEP_SCAN_DISABLE_REMOTE_CHECKS="${DEP_SCAN_DISABLE_REMOTE_CHECKS:-false}"
      --volume "$PWD:/code"
      --volume /var/run/docker.sock:/var/run/docker.sock
      "registry.gitlab.com/gitlab-org/security-products/dependency-scanning:$SP_VERSION" /code
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning-report.json
  only:
    - schedules

build client library:
  stage: prepare
  before_script:
    - cd lib/client
  script:
    - npm install
  except:
    - schedules
  cache:
    key: $CI_PIPELINE_ID-client
    policy: push
    paths:
      - lib/client/node_modules

build service library:
  stage: prepare
  before_script:
    - cd lib/service
  script:
    - npm install
  except:
    - schedules
  cache:
    key: $CI_PIPELINE_ID-service
    policy: push
    paths:
      - lib/service/node_modules

build integration tests:
  stage: prepare
  before_script:
    - cd integration-tests
  script:
    - npm install
  except:
    - schedules
  cache:
    key: $CI_PIPELINE_ID-integrational
    policy: push
    paths:
      - integration-tests/node_modules

lint client library:
  stage: tests
  before_script:
    - cd lib/client
    - npm install
  script:
    - npm run lint
  except:
    - schedules
  cache: *client-cache

lint service library:
  stage: tests
  before_script:
    - cd lib/service
    - npm install
  script:
    - npm run lint
  except:
    - schedules
  cache: *service-cache

lint integration tests:
  stage: tests
  before_script:
    - cd integration-tests
    - npm install
  script:
    - npm run lint
  except:
    - schedules
  cache: *integration-cache

test client library:
  stage: tests
  before_script:
    - cd lib/client
    - npm install
  script:
    - npm test
  except:
    - schedules
  cache: *client-cache

test service library:
  stage: tests
  before_script:
    - cd lib/service
    - npm install
  script:
    - npm test
  except:
    - schedules
  cache: *service-cache

test websocket:
  stage: integration tests
  before_script:
    - cd integration-tests
    - npm install
  script:
    - npm run test:ws
  except:
    - schedules
  cache: *integration-cache

test http:
  stage: integration tests
  before_script:
    - cd integration-tests
    - npm install
  script:
    - npm run test:http
  except:
    - schedules
  cache: *integration-cache

test amqp:
  stage: integration tests
  services:
    - rabbitmq:3.7
  before_script:
    - cd integration-tests
    - npm install
  script:
    - npm run test:amqp
  except:
    - schedules
  cache: *integration-cache

release client library:
  stage: release
  when: manual
  before_script:
    - cd lib/client
    - echo '//registry.npmjs.org/:_authToken=${NPM_TOKEN}' > .npmrc
  script:
    - npm publish
  only:
    - master
  except:
    - schedules

release service library:
  stage: release
  when: manual
  before_script:
    - cd lib/service
    - echo '//registry.npmjs.org/:_authToken=${NPM_TOKEN}' > .npmrc
  script:
    - npm publish
  only:
    - master
  except:
    - schedules
