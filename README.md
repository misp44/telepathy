# Telepathy

Telepathy is a lightweight Node.js library for remote procedure calls using proxy objects.

## Getting Started

To work with Telepathy you must have installed Node.js version 8 or higher.

Telepathy is able to communicate through HTTP, WebSockets or custom transports.


## Installation

```bash
npm install --save telepathy-server
```

```bash
npm install --save telepathy-client
```

## Usage

### Service

Let's write simple service:

```javascript
const {Service, Server, Transports: {WebSocketTransport}} = require('telepathy-server');

class DemoService extends Service {
	constructor() {
		// You have to define which methods should be exposed
		super({}, 'welcome');
	}

	welcome(name) {
	    return `Hello, ${name}.`;
	}
}

const service = new DemoService();
const transports = [new WebSocketTransport({port: 9001})];
const server = new Server(transports, service);

server.start();
```

### Client

```javascript
const {Client, Transports: {WebSocketTransport}} = require('telepathy-client');

(async () => {
    const transport = new WebSocketTransport();
    const client = new Client(transport);
    const service = client.getProxy();

    await transport.connect('ws://localhost:9001');
    
    console.log(await service.welcome('world'));
})();
```

## Contributing

Please read [contributing guideline](CONTRIBUTING.md) for details.

## Credits

- Patryk Miszczak @misp44 (author)

- Kacper Betański @azaradel

## License

This project is licensed under the [MIT License](LICENSE).
