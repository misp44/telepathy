/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const {expect} = require('chai');

const ADDRESS = 'amqp://rabbitmq';
const QUEUE = 'test-queue';

describe('AMQP Service', () => {
	let service, proxy;

	before(async () => {
		service = await _createService();
		proxy = await _createClient();
	});

	after(() => {
		service.stop();
	});

	it('should execute procedure remotely', async () => {
		const response = await proxy.add(4, 5);

		expect(response).to.be.equal(9);
	});

	it('should catch remote error', async () => {
		// Todo: Chai-as-promise
		let error;

		try {
			await proxy.bad();
		} catch (err) {
			error = err;
		}

		expect(error).to.have.property('message', 'Internal service error');
	});
});

async function _createService() {
	const { Service, Server, Transports } = require('telepathy-server');
	const { AmqpTransport } = Transports;

	class TestService extends Service {
		constructor() {
			super({}, 'add', 'bad');
		}

		async add(a, b) {
			return a + b;
		}

		async bad() {
			throw new Error('Some error');
		}
	}

	const service = new TestService();
	const transport = new AmqpTransport({url: ADDRESS, queue: QUEUE});
	const server = new Server([transport], service);

	await server.start();

	console.log('service started');

	return server;
}

async function _createClient() {
	const { Client, Transports } = require('telepathy-client');
	const { AmqpTransport } = Transports;

	const transport = new AmqpTransport();
	const client = new Client(transport);

	await transport.connect(ADDRESS, QUEUE);

	return client.getProxy();
}
