/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const {expect} = require('chai');

const PORT = 9000;
const ADDRESS = `ws://localhost:${PORT}`;

describe('Web Socket Service', () => {
	let service, proxy;

	before(async () => {
		service = await _createService(PORT);
		proxy = await _createClient(ADDRESS);
	});

	after(() => {
		service.stop();
	});

	it('should execute procedure remotely', async () => {
		const response = await proxy.add(4, 5);

		expect(response).to.be.equal(9);
	});

	it('should catch remote error', async () => {
		// Todo: Chai-as-promise
		let error;

		try {
			await proxy.bad();
		} catch (err) {
			error = err;
		}

		expect(error).to.have.property('message', 'Internal service error');
	});
});

async function _createService(port) {
	const { Service, Server, Transports } = require('telepathy-server');
	const { WebSocketTransport } = Transports;

	class TestService extends Service {
		constructor() {
			super({}, 'add', 'bad');
		}

		async add(a, b) {
			return a + b;
		}

		async bad() {
			throw new Error('Some error');
		}
	}

	const service = new TestService();
	const transport = new WebSocketTransport({port});
	const server = new Server([transport], service);

	await server.start();

	console.log('service started');

	return server;
}

async function _createClient(address) {
	const { Client, Transports } = require('telepathy-client');
	const { WebSocketTransport } = Transports;

	const transport = new WebSocketTransport();
	const client = new Client(transport);

	await transport.connect(address);

	return client.getProxy();
}
