/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const Service = require('../lib/service/src/service');
const Server = require('../lib/service/src/server');
const HttpTransport = require('../lib/service/src/transports/http');
const WebSocketTransport = require('../lib/service/src/transports/ws');
const AmqpTransport = require('../lib/service/src/transports/amqp');

class DemoService extends Service {
	constructor() {
		super({}, 'add', 'inc');

		this.i = 0;
	}

	async add(a, b) {
		return a + b;
	}

	async inc() {
		this.i = this.i + 1;

		return this.i;
	}
}

(async () => {
	try {
		const service = new DemoService();

		const transports = [
			new HttpTransport(9000),
			new WebSocketTransport({port: 9001}),
			new AmqpTransport({host: 'localhost', queue: 'ServiceQueue'})
		];

		const server = new Server(transports, service);

		await server.start();

		console.log('Server started');
	} catch (ex) {
		console.log(ex);
	}
})();
