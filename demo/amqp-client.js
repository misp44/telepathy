/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const Client = require('../lib/client/src/client');
const AmqpTransport = require('../lib/client/src/transports/amqp');

const transport = new AmqpTransport();
const client = new Client(transport);
const service = client.getProxy();

(async () => {
	try {
		await transport.connect('amqp://localhost', 'ServiceQueue');

		const incResponse = await service.inc();
		const addResponse = await service.add(incResponse, 5);

		console.log(addResponse);
	} catch (error) {
		console.error(error);
	}
})();
