/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const Client = require('../lib/client/src/client');
const HttpTransport = require('../lib/client/src/transports/http');

const transport = new HttpTransport();
const client = new Client(transport);
const service = client.getProxy();

(async () => {
	try {
		await transport.connect('localhost', 9000);

		const incResponse = await service.inc();
		const addResponse = await service.add(incResponse, 5);

		console.log(addResponse);
	} catch (error) {
		console.error(error);
	}
})();
