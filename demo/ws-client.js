/**
 * Copyright 2019 Patryk Miszczak
 */

'use strict';

const Client = require('../lib/client/src/client');
const WebSocketTransport = require('../lib/client/src/transports/ws');

const transport = new WebSocketTransport();
const client = new Client(transport);
const service = client.getProxy();

(async () => {
	try {
		await transport.connect('ws://localhost:9001');

		const incResponse = await service.inc();
		const addResponse = await service.add(incResponse, 5);

		console.log(addResponse);
	} catch (error) {
		console.error(error);
	}
})();
